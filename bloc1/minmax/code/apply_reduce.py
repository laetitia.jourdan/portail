#import reducer1 as reducer
# import reducer2  as reducer

def apply_reduce(list):
    '''
    @param {list(chaine)}  list : la liste des chaînes à manipuler
    @return {int} la valeur fournie par l'opération de reudction
    '''
    ready_to_transform = reducer.prepare(list)
    result = reducer.execute(ready_to_transform)
    return result




    
if __name__ == '__main__':
    import sys
    if len(sys.argv) < 2:
        module = 'reducer1'
    else:
        module = sys.argv[1]
    reducer = __import__(module)
    
    print( apply_reduce(["timoleon", "abracadabra", "banane", "abc"]) )

# fonctionx d'évaluation : Othello

Prenons l'exemple du jeu Othello pour présenter des exemples de fonctions d'évaluation :

 -   Le but du jeu est d'avoir à la fin plus de pions de sa couleur
     que de pions de l'adversaire. Il peut donc sembler naturel de
     favoriser les situations dans lesquelles notre nombre de pions est
     supérieur à celui de l'adversaire, et même de les favoriser
     d'autant plus que l'écart est en notre faveur. Une première
     fonction d'évaluation consiste donc simplement pour une situation
     donnée à calculer la différence entre le nombre de pions de sa
     couleur et le nombre de pions de l'adversaire et d'attribuer
     cette valeur à la situation :

      *eval(situation, joueur) = nombre_pions(situation, joueur) - nombre_pions(situation, adversaire)*

     Bien que rudimentaire, cette fonction d'évaluation permet déjà
     d'avoir un programme capable de joueur contre des débutants.

 -   La fonction précédente reste cependant très basique. On peut
     l'améliorer par l'analyse du jeu. Ceux qui ont déjà joué, même
     très peu, à Othello savent que certaines position du plateau de
     jeu sont meilleures que d'autres. C'est par exemple le cas des
     coins qui sont imprenables, ou des positions sur les bordures qui
     sont plus faciles à défendre que les positions du centre. Or la
     fonction d'évaluation précédente ne fait pas la distinction entre
     un pion au centre et un pion dans un coin. Pour contrer ce défaut,
     une solution est de ne pas se contenter de compter le nombre de
     pions de chaque couleur, mais d'attribuer en plus un *poids* à
     chaque case du plateau. Plus la case est intéressante, plus ce
     poids est important. Par exemple, les cases des coins pourraient
     avoir un poids de 20, celles des bordures de 5 et celles du centre
     de 1 (ces valeurs sont arbitraires, à vous de proposer les valeurs
     qui vous semblent pertinentes, sur plus de types de cases
     éventuellement). La fonction d'évaluation calcule cette fois la
     différence entre la somme des poids des cases occupées par les
     pions du joueur et la somme des poids des cases occupées par les
     pions adverses.

	 *valeur_joueur = somme des poids des cases occupées par le joueur*   
	 *valeur_adversaire = somme des poids des cases occupées par l'adversaire*   
	 *eval(situation, joueur) = valeur_joueur - valeur_adversaire*   
	 

     Avec une telle fonction l'algorithme aura tendance à favoriser
     les situations où les pions du joueur occupent des cases fortes,
     les coins en particulier.

     NB : la fonction précédente est une variante de cette fonction
     avec les poids de toutes les cases fixées à 1.

La seconde fonction étant plus performante, pour une même profondeur de
calcul, un algorithme *min-max* utilisant la seconde fonction jouera
mieux qu'un algorithme utilisant la première. Cette fonction reste
cependant encore très rudimentaire, d'autres fonctions d'évaluation
plus évoluées sont évidemment possibles pour le jeu d'Othello.

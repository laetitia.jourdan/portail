# Interface pour les  jeux à deux joueurs

## Algorithme

Rappelons l'algorithme proposé :

 1.  installer le jeu, c'est-à-dire créer la *situation courante* initiale  
 2.  déterminer le premier *joueur courant*  
 3.  si le jeu n'est pas fini   
    + si le *joueur courant* peut jouer dans la *situation courante*  
       + le *joueur courant* joue  
           c'est-à-dire qu'il choisit un coup possible parmi les coups autorisés dans la *situation courante* du jeu. Chaque coup de jeu amène le jeu dans une nouvelle situation. Donc choisir un coup de jeu (= jouer) revient à choisir la prochaine *situation courante* parmi toutes celles possibles.
       + mettre à jour la *situation courante* suite au choix du joueur
    + sinon  
      +  ne rien faire (la *situation courante* ne change pas)  
    + l'autre joueur devient le *joueur courant*  
    + recommencer en 3.   
 4.  sinon le jeu est fini    
    afficher le résultat (le joueur vainqueur ou partie nulle)

Cet algorithme est le même pour tous ces jeux. Les variations sont dues uniquement aux règles du jeu. Elles déterminent :

 1.  à l'étape 1, comment installer le jeu
 2.  à l'étape 3, quand le jeu se termine (c'est-à-dire quand la
     *situation courante* atteinte correspond à une fin de partie)
 3.  à l'étape 3	   , si le joueur peut jouer
 4.  à l'étape 3, quelles sont les prochaines situations suivantes
     possibles (ce sont celles parmi lesquelles le joueur doit
     choisir)
 5.  à l'étape 4, qui gagne ou pas

Grâce à une analyse descendante, on peut donc identifier pour chacun de ces points une fonction qu'il
faudra implémenter de manières différentes pour chacun des jeux que
l'on souhaite programmer. Dans l'ordre des points ci-dessus :

 1.  créer la situation initiale du jeu : `create_first_situation`
 2.  déterminer quand le jeu est fini, c'est-à-dire quand une situation est finale `is_game_finish`
 3.  déterminer si un joueur peut jouer `player_can_play`
 4.  calculer les situations filles `next_situations`
 5.  déterminer qui est vainqueur en fonction de la situation finale atteinte `get_winner`


On peut compléter ces fonctions par :

 -   une fonction pour afficher l'état du jeu, `display_game`
 -   une fonction pour permettre la saisie par le joueur humain de son coup de jeu, `human_player_play`  
	cela permet de traduire l'étape *"le* joueur courant
     *joue"* quand le joueur est un humain, une interaction avec le joueur doit alors être géré pour choisir la prochaine situation.


Il reste à identifier les paramètres de ces différentes méthodes, mais elles devront probablement au moins avoir comme paramètre la situation de jeu considérée. Le joueur courant devra sans doute être paramètre de certaines de ces fonctions.

Il faut donc pour chaque jeu, mettre en &oelig;uvre chacun de ces méthodes. Chaque doit être défini dans un module à part. Ces modules offrent donc les mêmes fonctions mais avec des implémentations différentes.
Il sera également définir nécessaire de définir, en plus, pour chaque jeu, la structure de données permettant de représenter une situation du jeu, ainsi que d'éventuelles fonctions pour la manipulation de cette structure de données.
Par exemple, pour le jeu de Nim, une situation peut être simplement représenter par le nombre d'allumettes posée sur la table. La structure est évidemment plus complexe pour l'Othello ou le puissance 4.


Cependant si la programmation de l'algorithme de jeu ci-dessus s'appuie sur ces noms de fonctions, il sera "facile" de changer le jeu joué en modifiant le jue importé.

	 